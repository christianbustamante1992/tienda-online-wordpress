<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XY?!NMK|JgNP`+_G|_la.EH+j@>t{|A7>;/cTDL%ol|4d!JQl@`d!Z*8-9m<!a8F' );
define( 'SECURE_AUTH_KEY',  'kIGZ?kB>RY8!N>$tCIkR%(A7wA?n0y44yz9[FMGjf:C&fu_]3-l#hK{l+0n?=krH' );
define( 'LOGGED_IN_KEY',    'tB>aZ$#@x&UTykU3Tga2A=GV!I/s?xP5j7QnHLbKbzBJ,2d96o1fwDMj2fQ!URj&' );
define( 'NONCE_KEY',        'Q36BLwrdov|U6s:]^0lkG1|pd=>!;L;|`UnE[pEHW}n(PJC eUco/;h)I2h)cgRI' );
define( 'AUTH_SALT',        'W-.xHz?xl@mhYb5>3~n|[:jMtRE;l]OottPtK $sh&M]2O|-)ZGjy.m{E.PKWx5^' );
define( 'SECURE_AUTH_SALT', '_&k|$a)nY27a9Q.%`/f|6dFIIjW59`LdKnIsl>q17UM&{mfEw3/:/x#J3]ka4=d?' );
define( 'LOGGED_IN_SALT',   'kO Zk1!&u>5I.C0N2I=1$z{`V]zaj|64}Z.:/66~1~ HU.|<Ql2rO`u9dg@SH6 ^' );
define( 'NONCE_SALT',       'OYw5zkprJi%s`vk/10UR%}B7Guk/2g!KI]%FM6*+;,ey4vA<l-xYq*-|^DCU-qU#' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
